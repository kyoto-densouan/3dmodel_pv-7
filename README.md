# README #

1/3スケールのカシオ PV-7風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK Fusion360です。


***

# 実機情報

## メーカ
- カシオ

## 発売時期
- 1984年10月15日

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/PV-7)
- [ボクたちが愛した、想い出のパソコン・マイコンたち](https://akiba-pc.watch.impress.co.jp/docs/column/retrohard/1099307.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pv-7/raw/24ea0584894cac2fa8c1fbb1f244195719db6cc3/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pv-7/raw/24ea0584894cac2fa8c1fbb1f244195719db6cc3/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pv-7/raw/24ea0584894cac2fa8c1fbb1f244195719db6cc3/ExampleImage.png)
